//Node
import React, { Component } from 'react';
import { PageHeader, Jumbotron } from 'react-bootstrap'
//Components
import Feed from './comp/feed-comp'
//Styles
import './lads-app.css';

export default class LadsApp extends Component {
  static defaultProps = {
		id: 2236
	}

	state = {
		title: '',
    description: '',
    entries:[]
	}

  componentDidMount(){
    fetch('https://b033nl7436.execute-api.eu-west-1.amazonaws.com/prod/api', {
    	method: 'get',
      mode: 'cors'
    }).then(resp => {
      return resp.json()
    }).then(data => {
      this.setState({
        ...data.feed
      })
    })
  }

	render() {
    let id = 0
		return (
			<div className="App">
				<PageHeader>{this.state.title}</PageHeader>
        <Jumbotron className="pad">
          <p>{this.state.description}</p>
        </Jumbotron>
        { this.state.entries.length !== 0 &&
          <div>
            { this.state.entries.map(f => {
              id++
              return (<Feed key={id} id={id} title={f.title} desc={f.contentSnippet} images={f.images} content={f.content} />)
            })}
          </div>
        }
			</div>
		)
	}
}
