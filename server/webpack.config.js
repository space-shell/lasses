const servWebpack = require("serverless-webpack");
const nodeExternals = require("webpack-node-externals");

module.exports = {
  entry: servWebpack.lib.entries,
  target: "node",
  externals: [nodeExternals()],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        include: `${__dirname}/src`,
        exclude: /node_modules/
      }
    ]
  }
};
